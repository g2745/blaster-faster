# script: enemy_clever

extends "res://scripts/enemy.gd"

const SCN_LASER: Resource = preload("res://scenes/laser_enemy.tscn")
const MAX_SCREEN_WIDTH: int = 16
const LASER_SHOOTING_TIME: float = 1.0

func _ready() -> void:
	velocity.x = utils.choose([velocity.x, -velocity.x])
	
	yield(utils.create_timer(1.5), "timeout")
	shoot()

func _process(delta) -> void:
	 # bouncing on the edges
	var right_screen: int = 0 + MAX_SCREEN_WIDTH
	if position.x <= right_screen:
		velocity.x = abs(velocity.x)
	
	var left_screen: int = utils.view_size.x - MAX_SCREEN_WIDTH
	if position.x >= left_screen:
		velocity.x = -abs(velocity.x)

func shoot() -> void:
	while true:
		var laser: Node = SCN_LASER.instance()
	
		laser.set_position(get_node("cannon").get_global_position())
		utils.main_node.add_child(laser)
		
		yield(utils.create_timer(LASER_SHOOTING_TIME), "timeout")
