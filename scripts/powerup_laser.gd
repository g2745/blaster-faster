# script: powerup_laser

extends "res://scripts/powerup.gd"

func _ready() -> void:
	connect("area_entered", self, "_on_area_entered")
	
func _on_area_entered(other) -> void:
	if other.is_in_group("ship"):
		other.is_double_shooting = true
		audio_player.play("aud_powerup")
		queue_free()