# script: audio_player

extends Node

func play(sample_name) -> void:
	get_node(sample_name).play()