# script: ship

extends Area2D

var ship_movement_speed: float = 0.2
var armor: int = 4 setget set_armor
var is_double_shooting: bool = false setget set_double_shooting

const SCN_LASER: Resource = preload("res://scenes/laser_ship.tscn")
const SCN_EXPLOSION: Resource = preload("res://scenes/explosion.tscn")
const SCN_FLASH: Resource = preload("res://scenes/flash.tscn")
const TRY_AGAIN_SCENE: String = "res://stages/TryAgainScreen.tscn"
const MAX_SCREEN_WIDTH: int = 16
const LASER_SCATTERING: int = 25
const MAX_DOUBLE_LASER_TIME: int = 5
const MAX_AMOR: int = 4

signal armor_changed

func _ready() -> void:
	set_process(true)
	add_to_group("ship")

func _process(delta) -> void:
	# tracking maouse
	var motion: int = (utils.mouse_pos.x - position.x) * ship_movement_speed
	translate(Vector2(motion, 0))

	# clamping to view, dont let the ship leaf the screen
	var pos: Vector2 = position
	var left_screen: int = 0 + MAX_SCREEN_WIDTH
	var right_screen:int = utils.view_size.x - MAX_SCREEN_WIDTH

	pos.x = clamp(pos.x, left_screen, right_screen)
	set_position(pos)

func shoot() -> void:
	var pos_left: Vector2 = get_node("cannons/left").get_global_position()
	var pos_right: Vector2 = get_node("cannons/right").get_global_position()

	create_laser(pos_left)
	create_laser(pos_right)

	if is_double_shooting:
		# create second lasers
		var laser_left: Node = create_laser(pos_left)
		var laser_right: Node = create_laser(pos_right)
		laser_left.velocity.x = -LASER_SCATTERING
		laser_right.velocity.x = LASER_SCATTERING

	yield(utils.create_timer(0.25), "timeout")

func _input(event) -> void:
	if event is InputEventMouseButton && event.is_pressed():
		if Input.is_mouse_button_pressed(BUTTON_LEFT):
			# yield(utils.create_timer(0.5), "timeout")
			shoot()

func set_armor(new_value) -> void:
	if new_value > MAX_AMOR:
		return

	if new_value < armor:
		audio_player.play("aud_hit_ship")
		utils.main_node.add_child(SCN_FLASH.instance())

	armor = new_value
	emit_signal("armor_changed", armor)

	if armor <= 0:
		create_explosion()
		queue_free()
		end_game()

func set_double_shooting(new_value) -> void:
	is_double_shooting = new_value

	if is_double_shooting:
		yield(utils.create_timer(MAX_DOUBLE_LASER_TIME), "timeout")
		is_double_shooting = false

func create_laser(pos) -> Node:
	var laser: Node = SCN_LASER.instance()
	laser.set_position(pos)
	utils.main_node.add_child(laser)

	return laser

func create_explosion() -> void:
	var explosion: Node = SCN_EXPLOSION.instance()
	explosion.set_position(position)
	utils.main_node.add_child(explosion)

func end_game() -> void:
	var score: Node = get_node("../hud/text_score/label")
	var game = get_node("/root/game")
	game.set_bestscore(int(score.text))

	get_tree().change_scene(TRY_AGAIN_SCENE)
