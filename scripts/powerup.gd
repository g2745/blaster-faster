# script: powerup

extends Area2D

var velocity: Vector2 = Vector2(0, 200)

const MAX_SCREEN_HEIGHT: int = 7

func _ready() -> void:
	set_process(true)

func _process(delta) -> void:
	translate(velocity * delta)
	
	# if powerup below the screen
	if position.y >= utils.view_size.y + MAX_SCREEN_HEIGHT:
		queue_free()