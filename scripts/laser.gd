# script: laser

extends Area2D

export var velocity: Vector2 = Vector2()
const SCN_FLARE: Resource = preload("res://scenes/flare.tscn")

func _ready() -> void:
	set_process(true)
	create_flare()
	
	yield(get_node("vis_notifier"), "screen_exited")
	queue_free()
	
func _process(delta) -> void:
	translate(velocity * delta)
	
func create_flare() -> void:
	var flare = SCN_FLARE.instance()
	
	flare.set_position(position)
	utils.main_node.add_child(flare)
